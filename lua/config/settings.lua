local change_font_size = require("config.utils").change_font_size
local o = vim.o
local g = vim.g

o.title = true
o.mouse = "a"
o.splitbelow = true
o.splitright = true
o.colorcolumn = "80"
o.termguicolors = true
o.incsearch = true
o.ignorecase = true
o.smartcase = true
o.cmdheight = 1
o.hidden = true
o.updatetime = 750
o.foldmethod = "syntax"
o.guifont = "BlexMono Nerd Font:h14"
o.showmode = false
o.completeopt = "menu,menuone,noselect"
g.mapleader = ","
g.floaterm_keymap_toggle = "<leader>tt"
g.neovide_input_use_logo = true

vim.cmd("colorscheme kanagawa")
vim.cmd(
	"au FileType javascript,javascriptreact,json,typescript,typescriptreact,astro,lua,prisma set expandtab tabstop=2 softtabstop=0 shiftwidth=2 smarttab"
)
vim.api.nvim_create_user_command("IncFont", function()
	change_font_size(o.guifont, 1)
end, {})
vim.api.nvim_create_user_command("DecFont", function()
	change_font_size(o.guifont, -1)
end, {})
