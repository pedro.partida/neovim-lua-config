local null_ls = require("null-ls")
null_ls.setup({
  sources = {
    null_ls.builtins.formatting.stylua,
    null_ls.builtins.diagnostics.eslint.with({
      command = "./node_modules/.bin/eslint",
    }),
    null_ls.builtins.code_actions.eslint.with({
      command = "./node_modules/.bin/eslint",
    }),
    null_ls.builtins.formatting.prettier.with({
      command = "./node_modules/.bin/prettier"
    }),
    null_ls.builtins.diagnostics.flake8.with({
      args = { "default", "--stdin-display-name", "$FILENAME", "-" },
    }),
    null_ls.builtins.formatting.black,
  },
})
