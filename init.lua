local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	{
		"nvim-treesitter/nvim-treesitter",
		build = function()
			vim.cmd("TSUpdate")
		end,
	},
	"neovim/nvim-lspconfig",
	"nvim-lua/plenary.nvim",
	"svermeulen/vimpeccable",
	"nvim-lualine/lualine.nvim",
	"jose-elias-alvarez/typescript.nvim",
	"hrsh7th/cmp-nvim-lsp",
	"hrsh7th/cmp-buffer",
	"hrsh7th/cmp-path",
	"hrsh7th/cmp-cmdline",
	"hrsh7th/nvim-cmp",
	{ "L3MON4D3/LuaSnip", version = "1.*", build = "make install_jsregexp" },
	"saadparwaiz1/cmp_luasnip",
	"jiangmiao/auto-pairs",
	"kyazdani42/nvim-web-devicons",
	"nvim-telescope/telescope.nvim",
	"tpope/vim-fugitive",
	"MunifTanjim/nui.nvim",
	"folke/neodev.nvim",
	"lukas-reineke/indent-blankline.nvim",
	{ "rebelot/kanagawa.nvim", lazy = false, priority = 1000 },
	"nkakouros-original/numbers.nvim",
	"voldikss/vim-floaterm",
	"Vimjas/vim-python-pep8-indent",
	"numToStr/Comment.nvim",
	"windwp/nvim-ts-autotag",
	"folke/trouble.nvim",
	"onsails/lspkind.nvim",
	"jose-elias-alvarez/null-ls.nvim",
	{ "nvim-neo-tree/neo-tree.nvim", branch = "v2.x" },
})

require("config.settings")
require("config.keymaps")
require("config.plugins")
